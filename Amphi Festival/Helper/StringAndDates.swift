//
//  StringAndDates.swift
//  Amphi Festival
//
//  Created by Joscha Wiedrich on 12.06.18.
//  Copyright © 2018 Joscha Wiedrich. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class StringDate {
    
    class func StringDate(stringDate: String) -> Date {
        let date = DateFormatter()
        date.timeZone = TimeZone(abbreviation: "UTC")
        date.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let d = date.date(from: stringDate)
        return d!
    }
    
    class func StringDateFromCoreData(stringDate: String) -> Date {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
        let date = formatter.date(from: stringDate)
        
        return date!
    }
    
    class func StringToDate(stringDate: String) -> Date {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = formatter.date(from: stringDate)
        
        return date!
    }
    
    class func firstCharacter(string: String) -> String{
        return String(describing: string.first)
    }
    
    class func GetOnlyDateFromString(date: Date) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let dateString = formatter.string(from: date)
        
        return dateString
    }
    
    class func CastDateFromCoreDataToString(date: Date) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        let timeString = formatter.string(from: date)
        
        return timeString
    }
    
    class func StringToDate(dateString: String) -> NSDate{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = formatter.date(from: dateString)! as NSDate
        
        return date
    }
    
    class func getCurrentDateAsString() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        let datestring = dateFormatter.string(from: date)
        return datestring
    }
    
    class func getAllDays() -> [Date]{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Informationen")
        var dateArray: [Date]?
        do{
            let result = try FestivalData().coreDataManager.managedObjectContext.fetch(request)
            for data in result as! [Informationen]{
                dateArray = Date().allDates(startDate: data.start!, endDate: data.end!)
            }
        }catch{
            
        }
        
        return dateArray!
    }
    
    class func getImageFromUrl(imageData: Data?) -> UIImage{
        guard let data: Data = imageData else{
            let image = #imageLiteral(resourceName: "emptyImage")
            return image
        }
        return UIImage(data: data)!
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf16) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType:  NSAttributedString.DocumentType.html], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

extension Date {
    
    func allDates(startDate: Date, endDate: Date) -> [Date] {
        var date = startDate
        var array: [Date] = []
        while date <= endDate {
            array.append(date)
            date = Calendar.current.date(byAdding: .day, value: 1, to: date)!
        }
        return array
    }
}
