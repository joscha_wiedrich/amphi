//
//  CoreDataHelper.swift
//  Amphi Festival
//
//  Created by Joscha Wiedrich on 12.06.18.
//  Copyright © 2018 Joscha Wiedrich. All rights reserved.
//

import Foundation
import CoreData

func saveContext(context: NSManagedObjectContext) -> Bool {
    
    do{
        try context.save()
        return true
    }catch{
        let fetchError = error as NSError
        print("Unable to store new rate")
        print("\(fetchError), \(fetchError.localizedDescription)")
        return false
    }
    
}

func whatBandPlaysAtThisTimer(time: String, buehne: String?) -> Bands?{
    
    var fetchResult: Bands?
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    let dateTime = dateFormatter.date(from: time)
    //request.predicate = NSPredicate(format: "(date >= %@) AND (date <= %@)", startDate, endDate)
    
    do{
        let fetchRequest = BandData().fetchedResultsController.fetchRequest
        fetchRequest.predicate = NSPredicate(format: "%@ >= playtimeStart", dateTime! as NSDate)
        if buehne != nil{
            fetchRequest.predicate = NSPredicate(format: "%@ == buehne", buehne!)
        }
        let tempFetchResult =  try BandData().coreDataManager.managedObjectContext.fetch(fetchRequest)
        fetchResult = tempFetchResult[0]
    }catch{
        
    }
    
    return fetchResult
}
