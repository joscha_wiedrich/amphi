//
//  SettingsViewController.swift
//  Amphi Festival
//
//  Created by Joscha Wiedrich on 12.06.18.
//  Copyright © 2018 Joscha Wiedrich. All rights reserved.
//

import UIKit
import CoreData

class SettingsViewController: UIViewController {

    @IBOutlet weak var tbMinutesForNotifications: UITextField!
    var bandData: NSFetchedResultsController? = MyBands().fetchedResultsController
    
    @IBAction func btClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btSave(_ sender: Any) {
        let newMinutes = Int(tbMinutesForNotifications.text!)
        
        UserDefaults.standard.set(newMinutes, forKey: "notificationTimer")
        
        if let myBands = bandData?.fetchedObjects{
            for band in myBands{
                let infoString = LocalNotification.createNotificationInfoString(bandName: band.bandName!, startTime: band.playtimeStart!, endTime: band.playtimeEnd!, buehne: band.buehne!)
                LocalNotification.setNotification(notificationsForBand: band.bandName!, notificiationAdditionalInfos: infoString, notificationTime: band.playtimeStart!)          }
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tbMinutesForNotifications.text = String(describing: UserDefaults.standard.integer(forKey: "notificationTimer"))
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

}
