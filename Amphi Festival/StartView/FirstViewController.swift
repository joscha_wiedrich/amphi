//
//  FirstViewController.swift
//  Amphi Festival
//
//  Created by Joscha Wiedrich on 12.06.18.
//  Copyright © 2018 Joscha Wiedrich. All rights reserved.
//

import UIKit
import CoreData

class FirstViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate{

    @IBOutlet weak var lbTimer: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var bandData = MyBands().fetchedResultsController
    var festivalData = FestivalData().fetchedResultsController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.layer.borderWidth = 0.5
        tableView.layer.borderColor = UIColor.black.cgColor
        
        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(CountdownTimer), userInfo: nil, repeats: true)
        
        let spinnerView = FirstViewController.displaySpinner(onView: self.view)
        LoadFestivalsData.loadFestivals(apiString: "https://www.splitterkultur.de/FestivalApi/amphi.json", completion: { value  in
            FirstViewController.removeSpinner(spinner: spinnerView)
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        do{
            try bandData.performFetch()
        }catch{
            
        }
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sections = bandData.sections else{
            return 0
        }
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = bandData.sections else{
            return 0
        }
        
        let sectionInfo = sections[section]
        return sectionInfo.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let sections = bandData.sections else{
            return nil
        }
        
        let sectionInfo = sections[section]
        return sectionInfo.name
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let value = bandData.object(at: indexPath)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyBandsTableViewCell", for: indexPath) as! MyBandsTableViewCell
        cell.lbStartMyBand.text = value.bandName
        return cell
        
    }
    
    @objc func CountdownTimer() {
        let date = NSDate()
        let calendar = Calendar.current
        
        let components = calendar.dateComponents([.hour, .minute, .second,  .month, .year, .day], from: date as Date)
        
        let currentDate = calendar.date(from: components)
        
        let userCalendar = Calendar.current
        
        // here we set the due date. When the timer is supposed to finish
        let competitionDate = NSDateComponents()
        competitionDate.year = 2018
        competitionDate.month = 7
        competitionDate.day = 28
        competitionDate.hour = 12
        competitionDate.minute = 00
        competitionDate.second = 00
        let competitionDay = userCalendar.date(from: competitionDate as DateComponents)!
        
        //here we change the seconds to hours,minutes and days
        let CompetitionDayDifference = calendar.dateComponents([.day, .hour, .minute, .second], from: currentDate!, to: competitionDay)
        
        
        //finally, here we set the variable to our remaining time
        let daysLeft = CompetitionDayDifference.day!
        let hoursLeft = CompetitionDayDifference.hour!
        let minutesLeft = CompetitionDayDifference.minute!
        let secondsLeft = CompetitionDayDifference.second!
        
        lbTimer.text = ("Noch \(daysLeft) Tage \(hoursLeft) Std \(minutesLeft) Min \(secondsLeft) Sek")
    }
    
}

class ProgressHUD: UIVisualEffectView {
    
    var text: String? {
        didSet {
            label.text = text
        }
    }
    
    let activityIndictor: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    let label: UILabel = UILabel()
    let blurEffect = UIBlurEffect(style: .light)
    let vibrancyView: UIVisualEffectView
    
    init(text: String) {
        self.text = text
        self.vibrancyView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: blurEffect))
        super.init(effect: blurEffect)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.text = ""
        self.vibrancyView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: blurEffect))
        super.init(coder: aDecoder)
        self.setup()
    }
    
    func setup() {
        contentView.addSubview(vibrancyView)
        contentView.addSubview(activityIndictor)
        contentView.addSubview(label)
        activityIndictor.startAnimating()
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        if let superview = self.superview {
            
            let width = superview.frame.size.width / 2.3
            let height: CGFloat = 50.0
            self.frame = CGRect(x: superview.frame.size.width / 2 - width / 2,
                                y: superview.frame.height / 2 - height / 2,
                                width: width,
                                height: height)
            vibrancyView.frame = self.bounds
            
            let activityIndicatorSize: CGFloat = 40
            activityIndictor.frame = CGRect(x: 5,
                                            y: height / 2 - activityIndicatorSize / 2,
                                            width: activityIndicatorSize,
                                            height: activityIndicatorSize)
            
            layer.cornerRadius = 8.0
            layer.masksToBounds = true
            label.text = text
            label.textAlignment = NSTextAlignment.center
            label.frame = CGRect(x: activityIndicatorSize + 5,
                                 y: 0,
                                 width: width - activityIndicatorSize - 15,
                                 height: height)
            label.textColor = UIColor.white
            label.font = UIFont.boldSystemFont(ofSize: 16)
        }
    }
    
    func show() {
        self.isHidden = false
    }
    
    func hide() {
        self.isHidden = true
    }
}

extension UIViewController{
    class func displaySpinner(onView: UIView) -> UIView{
        let spinnerView = UIImageView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.black
        spinnerView.image = #imageLiteral(resourceName: "amphi.jpg")
        spinnerView.contentMode = .scaleAspectFit
        let progressHUD = ProgressHUD(text: "Aktualisiere die Daten")
        
        DispatchQueue.main.async {
            spinnerView.addSubview(progressHUD)
            onView.addSubview(spinnerView)
        }
        
        return spinnerView
    }
    
    class func removeSpinner(spinner: UIView){
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }

}

@IBDesignable extension UIButton {
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}
