//
//  CoreDataQueries.swift
//  Amphi Festival
//
//  Created by Joscha Wiedrich on 12.06.18.
//  Copyright © 2018 Joscha Wiedrich. All rights reserved.
//

import Foundation
import CoreData

open class FestivalData{
    
    let coreDataManager = CoreDataManager(modelName: "Amphi_Festival")
    
    lazy var fetchedResultsController: NSFetchedResultsController<Informationen> = {
        let fetchRequest: NSFetchRequest<Informationen> = Informationen.fetchRequest()
        
        let sortDescriptor = NSSortDescriptor(key: "title", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: self.coreDataManager.managedObjectContext,
                                                                  sectionNameKeyPath: nil,
                                                                  cacheName: nil)
        do{
            try fetchedResultsController.performFetch()
        }catch{
            let fetchError = error as NSError
            print("Unable to load rates")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        
        return fetchedResultsController
    }()
    
    func isRateDataSourceEmpty() -> Bool{
        if fetchedResultsController.fetchedObjects?.count == 0{
            return true
        }
        return false
    }
    
}

open class BandData{
    
    let coreDataManager = CoreDataManager(modelName: "Amphi_Festival")
    
    
    lazy var fetchedResultsController: NSFetchedResultsController<Bands> = {
        let fetchRequest: NSFetchRequest<Bands> = Bands.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "bandName", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: self.coreDataManager.managedObjectContext,
                                                                  sectionNameKeyPath: nil,
                                                                  cacheName: nil)
        do{
            try fetchedResultsController.performFetch()
        }catch{
            let fetchError = error as NSError
            print("Unable to load rates")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        
        return fetchedResultsController
    }()
    
    
}

open class BandDataFriday{
    
    let coreDataManager = CoreDataManager(modelName: "Amphi_Festival")
    
    
    lazy var fetchedResultsController: NSFetchedResultsController<Bands> = {
        let daytoFetch = "Friday"
        let fetchRequest: NSFetchRequest<Bands> = Bands.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "buehne", ascending: true)
        let secondSort = NSSortDescriptor(key: "playtimeStart", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor, secondSort]
        //fetchRequest.predicate = NSPredicate(format: "playtimeStart.sectionIdentifier == %@", day)
        fetchRequest.predicate = NSPredicate(format: "%K = %@", "day", "Friday")
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: self.coreDataManager.managedObjectContext,
                                                                  sectionNameKeyPath: "buehne",
                                                                  cacheName: nil)
        do{
            try fetchedResultsController.performFetch()
        }catch{
            let fetchError = error as NSError
            print("Unable to load rates")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        
        return fetchedResultsController
    }()
    
    
}

open class BandDataSaturday{
    
    let coreDataManager = CoreDataManager(modelName: "Amphi_Festival")
    
    lazy var fetchedResultsController: NSFetchedResultsController<Bands> = {
        let dayToFetch = "Saturday"
        let fetchRequest: NSFetchRequest<Bands> = Bands.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "buehne", ascending: true)
        let secondSort = NSSortDescriptor(key: "playtimeStart", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor, secondSort]
        fetchRequest.predicate = NSPredicate(format: "%K = %@", "day", "Saturday")
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: self.coreDataManager.managedObjectContext,
                                                                  sectionNameKeyPath: "buehne",
                                                                  cacheName: nil)
        do{
            try fetchedResultsController.performFetch()
        }catch{
            let fetchError = error as NSError
            print("Unable to load rates")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        
        return fetchedResultsController
    }()
    
}

open class BandDataSunday{
    
    let coreDataManager = CoreDataManager(modelName: "Amphi_Festival")
    
    lazy var fetchedResultsController: NSFetchedResultsController<Bands> = {
        let dayTofetch = "Sunday"
        let fetchRequest: NSFetchRequest<Bands> = Bands.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "buehne", ascending: true)
        let secondSort = NSSortDescriptor(key: "playtimeStart", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor, secondSort]
        fetchRequest.predicate = NSPredicate(format: "%K = %@", "day", "Sunday")
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: self.coreDataManager.managedObjectContext,
                                                                  sectionNameKeyPath: "buehne",
                                                                  cacheName: nil)
        do{
            try fetchedResultsController.performFetch()
        }catch{
            let fetchError = error as NSError
            print("Unable to load rates")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        
        return fetchedResultsController
    }()
    
}
extension Date {
    var sectionIdentifier: String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .iso8601)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: self)
    }
}

open class BandDataByDays{
    
    let coreDataManager = CoreDataManager(modelName: "Amphi_Festival")
    
    
    
    lazy var fetchedResultsController: NSFetchedResultsController<Bands> = {
        let fetchRequest: NSFetchRequest<Bands> = Bands.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "playtimeStart.sectionIdentifier", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: self.coreDataManager.managedObjectContext,
                                                                  sectionNameKeyPath: "playtimeStart",
                                                                  cacheName: nil)
        do{
            try fetchedResultsController.performFetch()
        }catch{
            let fetchError = error as NSError
            print("Unable to load rates")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        
        return fetchedResultsController
    }()
}

open class BandByMainstage{
    
    let coreDataManager = CoreDataManager(modelName: "Amphi_Festival")
    
    
    
    lazy var fetchedResultsController: NSFetchedResultsController<Bands> = {
        let fetchRequest: NSFetchRequest<Bands> = Bands.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "buehne", ascending: true)
        let secondSortDescriptor = NSSortDescriptor(key: "playtimeStart", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor, secondSortDescriptor]
        fetchRequest.predicate = NSPredicate(format: "buehne = 'Mainstage'")
        
        
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: self.coreDataManager.managedObjectContext,
                                                                  sectionNameKeyPath: "day",
                                                                  cacheName: nil)
        do{
            try fetchedResultsController.performFetch()
        }catch{
            let fetchError = error as NSError
            print("Unable to load rates")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        
        return fetchedResultsController
    }()
}

open class BandBySecondstage{
    
    let coreDataManager = CoreDataManager(modelName: "Amphi_Festival")
    
    
    
    lazy var fetchedResultsController: NSFetchedResultsController<Bands> = {
        let fetchRequest: NSFetchRequest<Bands> = Bands.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "buehne", ascending: true)
        let secondSortDescriptor = NSSortDescriptor(key: "playtimeStart", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor, secondSortDescriptor]
        fetchRequest.predicate = NSPredicate(format: "buehne = 'Theater Stage'")
        
        
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: self.coreDataManager.managedObjectContext,
                                                                  sectionNameKeyPath: "day",
                                                                  cacheName: nil)
        do{
            try fetchedResultsController.performFetch()
        }catch{
            let fetchError = error as NSError
            print("Unable to load rates")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        
        return fetchedResultsController
    }()
}

open class BandByThirdstage{
    
    let coreDataManager = CoreDataManager(modelName: "Amphi_Festival")
    
    
    
    lazy var fetchedResultsController: NSFetchedResultsController<Bands> = {
        let fetchRequest: NSFetchRequest<Bands> = Bands.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "buehne", ascending: true)
        let secondSortDescriptor = NSSortDescriptor(key: "playtimeStart", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor, secondSortDescriptor]
        fetchRequest.predicate = NSPredicate(format: "buehne = 'Orbit Stage'")
        
        
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: self.coreDataManager.managedObjectContext,
                                                                  sectionNameKeyPath: "day",
                                                                  cacheName: nil)
        do{
            try fetchedResultsController.performFetch()
        }catch{
            let fetchError = error as NSError
            print("Unable to load rates")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        
        return fetchedResultsController
    }()
}

open class MyBands{
    let coreDataManager = CoreDataManager(modelName: "Amphi_Festival")
    
    
    
    lazy var fetchedResultsController: NSFetchedResultsController<Bands> = {
        let fetchRequest: NSFetchRequest<Bands> = Bands.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "day", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.predicate = NSPredicate(format: "setNotification == %@", NSNumber(value: true))
        
        
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: self.coreDataManager.managedObjectContext,
                                                                  sectionNameKeyPath: "day",
                                                                  cacheName: nil)
        do{
            try fetchedResultsController.performFetch()
        }catch{
            let fetchError = error as NSError
            print("Unable to load rates")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        
        return fetchedResultsController
    }()
}
