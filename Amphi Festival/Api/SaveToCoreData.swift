//
//  SaveToCoreData.swift
//  Amphi Festival
//
//  Created by Joscha Wiedrich on 12.06.18.
//  Copyright © 2018 Joscha Wiedrich. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class FestivalDataSaveAndLoad {
    
    class func NewFestivalInfos(festival: Festival, bands: [Band]){
        
        let festivalContext = FestivalData().coreDataManager.managedObjectContext
        let bandContext = BandData().coreDataManager.managedObjectContext
        
        guard let oldFestivalInfos = LoadFestivalInfosFromCoreData(managedContext: festivalContext) else{
            clearEntity(entityName: "Informationen", context: festivalContext)
            clearEntity(entityName: "Bands", context: bandContext)
            createNewEntryForFestivalInfos(newValue: festival, entityName: "Informationen", context: festivalContext)
            createNewEntryForBand(newValue: festival.bands, entityName: "Bands", context: bandContext)
            return
        }
        
        if checkVersionNummerFromJson(newVersionNumber: Double(festival.version)!, oldVersionNumber: oldFestivalInfos[0].version){
            clearEntity(entityName: "Informationen", context: festivalContext)
            clearEntity(entityName: "Bands", context: bandContext)
            createNewEntryForFestivalInfos(newValue: festival, entityName: "Informationen", context: festivalContext)
            createNewEntryForBand(newValue: festival.bands, entityName: "Bands", context: bandContext)
        }
        
    }
    
    class func checkVersionNummerFromJson(newVersionNumber: Double, oldVersionNumber: Double?) -> Bool {
        
        guard let oldVersion = oldVersionNumber else{
            return false
        }
        
        if newVersionNumber > oldVersion{
            return true
        }
        
        return false
    }
    
    class func createNewEntryForFestivalInfos(newValue: Festival, entityName: String, context: NSManagedObjectContext){
        
        let newFestivalInfos = NSEntityDescription.insertNewObject(forEntityName: entityName, into: context) as! Informationen
        newFestivalInfos.title = newValue.title
        newFestivalInfos.version = Double(newValue.version)!
        newFestivalInfos.start = StringDate.StringDate(stringDate: newValue.festivalStart)
        newFestivalInfos.end = StringDate.StringDate(stringDate: newValue.festivalEnd)
        newFestivalInfos.poster = downloadImage(urlString: newValue.poster)
        newFestivalInfos.banner = downloadImage(urlString: newValue.banner)
        if saveContext(context: context){
            print("Saved Festival")
        }
    }
    
    class func createNewEntryForBand(newValue: [Band], entityName: String, context: NSManagedObjectContext){
        
        for band in newValue{
            
            let newBand = NSEntityDescription.insertNewObject(forEntityName: entityName, into: context) as! Bands
            newBand.bandName = band.bandname
            newBand.buehne = band.buehne
            newBand.bandInfo = band.bandinfos
            newBand.playtimeStart = StringDate.StringToDate(stringDate: band.playtimeStart!)
            newBand.playtimeEnd = StringDate.StringToDate(stringDate: band.playtimeEnd!)
            newBand.bandImage = downloadImage(urlString: band.bandimage)
            newBand.day = band.day
            
            if saveContext(context: context){
                print("Saved Band")
            }
            
        }
        
    }
    
    class func downloadImage(urlString: String?) -> Data?{
        
        guard let url = urlString else{
            return nil
        }
        if url == ""{
            return nil
        }
        let parsedUrl = URL(string: url)
        let data = try? Data(contentsOf: parsedUrl!)
        if data != nil{
            return data
        }
        return nil
    }
    
    class func clearEntity(entityName: String, context: NSManagedObjectContext){
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        do{
            try context.execute(request)
        }catch{
            print("Can´t delete \(entityName) values")
        }
    }
    
    // Load general festival informations
    class func LoadFestivalInfosFromCoreData(managedContext: NSManagedObjectContext) -> [Informationen]? {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Informationen")
        var result = [Informationen]()
        
        do{
            let records = try managedContext.fetch(fetchRequest)
            if let records = records as? [Informationen]{
                result = records
            }
        }catch{
            
        }
        
        if result.count == 0{
            return nil
        }
        else{
            return result
        }
        
    }
    
    // Load general festival informations
    class func LoadBandInfosFromCoreData(managedContext: NSManagedObjectContext) -> [Bands] {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Bands")
        var result = [Bands]()
        
        do{
            let records = try managedContext.fetch(fetchRequest)
            if let records = records as? [Bands]{
                result = records
            }
        }catch{
            
        }
        
        return result
    }
}
