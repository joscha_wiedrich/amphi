//
//  LoadFromApi.swift
//  Amphi Festival
//
//  Created by Joscha Wiedrich on 12.06.18.
//  Copyright © 2018 Joscha Wiedrich. All rights reserved.
//

import Foundation
import  UIKit

class LoadFestivalsData{
    
    class func loadFestivals(apiString: String, completion: @escaping (Bool)->()){
        
        //var latestPost = [Festival]()
        
        let jsonUrl = apiString
        let url = URL(string: jsonUrl)!
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if error != nil {
                print("Error URLSession : \(error!)")
                completion(false)
            }
            guard let data = data else { completion(false); return }
            do{
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .iso8601
                let festivalInfosComplete = try decoder.decode(Festival.self, from: data)
                FestivalDataSaveAndLoad.NewFestivalInfos(festival: festivalInfosComplete, bands: festivalInfosComplete.bands)
                completion(true)
                print("completion is true")
                
            }catch{
                print(error)
                completion(false)
            }
            }.resume()
        
        
        /*URLSession.shared.dataTask(with: url!){(data, response, error) in
         do{
         latestPost = try JSONDecoder().decode([Festival].self, from: data!)
         
         if SaveAndLoadFromCoreData.checkIfPostsExists(id: (latestPost.first?.id)!) == false{
         for eachPost in latestPost{
         SaveAndLoadFromCoreData.save(id: eachPost.id, content: eachPost.content.rendered, modified: StringDate.StringDate(stringDate: eachPost.modified), slug: eachPost.slug)
         }
         }
         }catch{
         print(error)
         }
         }.resume()*/
    }
    
    /*class func loadAmphiData (){
     loadFestivals(apiString: "https://www.splitterkultur.de/FestivalApi/amphi.json")
     }*/
    
    
}
