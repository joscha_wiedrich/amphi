//
//  AmphiStruct.swift
//  Amphi Festival
//
//  Created by Joscha Wiedrich on 12.06.18.
//  Copyright © 2018 Joscha Wiedrich. All rights reserved.
//

import Foundation

struct Festival:Codable{
    let title: String
    let description: String
    let version: String
    let festivalStart: String
    let festivalEnd: String
    let poster: String
    let banner: String
    let bands: [Band]
}

struct Band:Codable{
    let bandname: String
    let buehne: String?
    let playtimeStart: String?
    let playtimeEnd: String?
    let bandinfos: String?
    let bandimage: String?
    let day: String?
}
