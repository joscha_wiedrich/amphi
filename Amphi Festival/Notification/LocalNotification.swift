//
//  LocalNotification.swift
//  Amphi Festival
//
//  Created by Joscha Wiedrich on 12.06.18.
//  Copyright © 2018 Joscha Wiedrich. All rights reserved.
//

import Foundation
import UserNotifications

class LocalNotification {
    
    class func checkNotificationAuthorization() -> Bool {
        let center = UNUserNotificationCenter.current()
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        
        center.requestAuthorization(options: options){
            (granted, error) in
            if !granted {
                print("Something went wrong")
            }
        }
        
        center.getNotificationSettings{ (settings) in
            if settings.authorizationStatus != .authorized{
                print("Notifications not allowed")
            }
        }
        
        return true
    }
    
    class func setNotification (notificationsForBand: String, notificiationAdditionalInfos: String, notificationTime: Date) {
        
        let timer = UserDefaults.standard.integer(forKey: "notificationTimer")
        
        let notificationAllowed = checkNotificationAuthorization()
        
        if(notificationAllowed){
            
            let center = UNUserNotificationCenter.current()
            
            //Notification Content
            let content = UNMutableNotificationContent()
            content.title = notificationsForBand
            content.body = notificiationAdditionalInfos
            content.sound = UNNotificationSound.default()
            
            //Notification Trigger - PlayTime minus minutes from userstore
            let calendar = Calendar.current
            let newDate = calendar.date(byAdding: .minute, value: -timer, to: notificationTime)
            let triggerDate = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: newDate!)
            let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
            
            let identifier = notificationsForBand
            let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
            center.add(request, withCompletionHandler: {(error) in
                if let error = error{
                    print(error)
                }
            })
            
        }
        
        
    }
    
    class func createNotificationInfoString(bandName: String, startTime: Date, endTime: Date, buehne: String) -> String{
        let start = StringDate.CastDateFromCoreDataToString(date: startTime)
        let end = StringDate.CastDateFromCoreDataToString(date: endTime)
        let infoString = bandName + " spielt gleich von " + start + " bis " + end + " auf der " + buehne
        
        return infoString
    }
    
    class func deleteNotification(notificationsForBand: String){
        let center = UNUserNotificationCenter.current()
        center.removePendingNotificationRequests(withIdentifiers: [notificationsForBand])
    }
    
}
