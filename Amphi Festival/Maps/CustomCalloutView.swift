//
//  CustomCalloutView.swift
//  Amphi Festival
//
//  Created by Joscha Wiedrich on 12.06.18.
//  Copyright © 2018 Joscha Wiedrich. All rights reserved.
//

import UIKit

class CustomCalloutView: UIView {
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbCurrentBand: UILabel?
    @IBOutlet weak var lbCurrentStart: UILabel?
    @IBOutlet weak var lbCurrentEnd: UILabel?
    @IBOutlet weak var imageView: UIImageView!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
