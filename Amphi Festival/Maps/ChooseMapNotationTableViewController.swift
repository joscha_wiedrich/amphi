//
//  ChooseMapNotationTableViewController.swift
//  Amphi Festival
//
//  Created by Joscha Wiedrich on 12.06.18.
//  Copyright © 2018 Joscha Wiedrich. All rights reserved.
//

import UIKit

protocol PlacesArrayDelegate: class {
    func setAnnotationValues(arrayOfSelectedValues: [String])
}

class ChooseMapNotationTableViewController: UITableViewController {

    weak var delegate: PlacesArrayDelegate? = nil
    
    var places: [String] = ["Buehne", "EinAusgang", "WC", "Parkplatz"]
    var selectedPlaces: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedPlaces = UserDefaults.standard.array(forKey: "selectedLocations") as! [String]
        delegate?.setAnnotationValues(arrayOfSelectedValues: selectedPlaces)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        cell.textLabel?.text = places[indexPath.row]
        cell.accessoryType = selectedPlaces.contains((cell.textLabel?.text)!) ? .checkmark: .none
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) else {return}
        let place = places[indexPath.row]
        
        if cell.accessoryType == .checkmark{
            selectedPlaces = selectedPlaces.filter {$0 != place}
            cell.accessoryType = .none
        } else{
            selectedPlaces += [place]
            cell.accessoryType = .checkmark
        }
        UserDefaults.standard.set(selectedPlaces, forKey: "selectedLocations")
        delegate?.setAnnotationValues(arrayOfSelectedValues: selectedPlaces)
    }
    
    func changeName(value: String) -> String{
        switch value {
        case "Bühnen":
            return "Buehnen"
        case "Ein-/Ausgänge":
            return "EinAusgang"
        case "WC´s":
            return "WC"
        default:
            return ""
        }
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

}
