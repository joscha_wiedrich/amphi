//
//  MapViewController.swift
//  Amphi Festival
//
//  Created by Joscha Wiedrich on 12.06.18.
//  Copyright © 2018 Joscha Wiedrich. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, PlacesArrayDelegate {

    @IBOutlet weak var mapView: MKMapView!
   
    @objc func mapTypeChanged(segment: UISegmentedControl) {
        mapView.mapType = MKMapType.init(rawValue: UInt(segment.selectedSegmentIndex)) ?? .standard
    }
    let locationManager = CLLocationManager()
    var places: [Places]!
    var selectedPlaces: [Places]?
    var selectedGroups: [String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let segment: UISegmentedControl = UISegmentedControl(items: ["Karte", "Satellit", "Hybrid"])
        segment.sizeToFit()
        segment.selectedSegmentIndex = 0
        self.navigationItem.titleView = segment
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .organize, target: self, action: #selector(viewToChoose(sender:)))
        segment.addTarget(self, action: #selector(mapTypeChanged(segment:)), for: .valueChanged)
        
        
        if UserDefaults.standard.array(forKey: "selectedLocations") == nil{
            places = Places.getPlaces()
        }else{
            selectedGroups = UserDefaults.standard.array(forKey: "selectedLocations") as? [String]
            setAnnotationValues(arrayOfSelectedValues: selectedGroups!)
        }
        
        requestLocationAcces()
        addNotation()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @objc func addTapped(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func addNotation(){
        mapView.delegate = self
        createNewMap(newNotations: places)
        zoomMapaFitAnnotations()
    }
    
    func requestLocationAcces(){
        let status = CLLocationManager.authorizationStatus()
        
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            return
        case .denied, .restricted:
            print("location access denied")
        default:
            locationManager.requestWhenInUseAuthorization()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setAnnotationValues(arrayOfSelectedValues: UserDefaults.standard.array(forKey: "selectedLocations") as! [String])
    }
    
    func zoomMapaFitAnnotations() {
        
        var zoomRect = MKMapRectNull
        for annotation in mapView.annotations {
            
            let annotationPoint = MKMapPointForCoordinate(annotation.coordinate)
            
            let pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0)
            
            if (MKMapRectIsNull(zoomRect)) {
                zoomRect = pointRect
            } else {
                zoomRect = MKMapRectUnion(zoomRect, pointRect)
            }
        }
        self.mapView.setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsets(top: 50, left: 50, bottom: 50, right: 50), animated: true)
        
    }
    
    func setAnnotationValues(arrayOfSelectedValues: [String]){
        var newPlaces: [Places] = []
        places = Places.getPlaces()
        for selValue in arrayOfSelectedValues{
            let value = places.filter {$0.groupName!.contains(selValue)}
            newPlaces.append(contentsOf: value)
            
        }
        selectedPlaces = newPlaces
        mapView.removeAnnotations(mapView.annotations)
        createNewMap(newNotations: selectedPlaces!)
    }
    
    @objc func viewToChoose(sender: UIBarButtonItem){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ChooseMapNotationTableViewController") as! ChooseMapNotationTableViewController
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let chooseView = segue.destination as! ChooseMapNotationTableViewController
        chooseView.delegate = self
    }
    
    func createNewMap(newNotations: [Places]){
        print(newNotations.count)
        mapView.addAnnotations(newNotations)
    }
    
}

extension MapViewController: MKMapViewDelegate {
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? Places else { return nil }
        let identifier = "marker"
        var view: MKMarkerAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
            let color = setPinColorForGroup(groupName: annotation.groupName!)
            view.markerTintColor = color.0
            view.glyphImage = color.1
        } else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = false
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            let color = setPinColorForGroup(groupName: annotation.groupName!)
            view.markerTintColor = color.0
            view.glyphImage = color.1
            
        }
        return view
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if view.annotation is MKUserLocation{
            return
        }
        
        let festivalAnnotation = view.annotation as! Places
        let views = Bundle.main.loadNibNamed("CustomCalloutView", owner: nil, options: nil)
        let calloutView = views?[0] as! CustomCalloutView
        calloutView.layer.shadowColor = UIColor.black.cgColor
        calloutView.layer.shadowOpacity = 1.0
        calloutView.layer.shadowOffset = CGSize.zero
        calloutView.layer.shadowRadius = 10
        
        if festivalAnnotation.title == "Mainstage" || festivalAnnotation.title == "Theater Stage" || festivalAnnotation.title == "Orbit Stage"{
            
            //TODO: time variable muss noch auf Aktuelle Zeit umgestellt werden
            if(festivalAnnotation.title == "Mainstage"){
                let currentBand = whatBandPlaysAtThisTimer(time: "2018-07-28 09:00", buehne: festivalAnnotation.title)
                calloutView.lbCurrentBand?.text = currentBand?.bandName
                calloutView.lbCurrentStart?.text = StringDate.CastDateFromCoreDataToString(date: (currentBand?.playtimeStart)!)
                calloutView.lbCurrentEnd?.text = StringDate.CastDateFromCoreDataToString(date: (currentBand?.playtimeEnd)!)
                calloutView.imageView.image = StringDate.getImageFromUrl(imageData: currentBand?.bandImage)
                
            }
            if(festivalAnnotation.title == "Theater Stage"){
                let currentBand = whatBandPlaysAtThisTimer(time: "2018-07-28 09:00", buehne: festivalAnnotation.title)
                calloutView.lbCurrentBand?.text = currentBand?.bandName
                calloutView.lbCurrentStart?.text = StringDate.CastDateFromCoreDataToString(date: (currentBand?.playtimeStart)!)
                calloutView.lbCurrentEnd?.text = StringDate.CastDateFromCoreDataToString(date: (currentBand?.playtimeEnd)!)
                calloutView.imageView.image = StringDate.getImageFromUrl(imageData: currentBand?.bandImage)
                
            }
            if(festivalAnnotation.title == "Orbit Stage"){
                let currentBand = whatBandPlaysAtThisTimer(time: "2018-07-28 09:00", buehne: festivalAnnotation.title)
                calloutView.lbCurrentBand?.text = currentBand?.bandName
                calloutView.lbCurrentStart?.text = StringDate.CastDateFromCoreDataToString(date: (currentBand?.playtimeStart)!)
                calloutView.lbCurrentEnd?.text = StringDate.CastDateFromCoreDataToString(date: (currentBand?.playtimeEnd)!)
                calloutView.imageView.image = StringDate.getImageFromUrl(imageData: currentBand?.bandImage)
                
            }
            calloutView.lbTitle.text = festivalAnnotation.title
            calloutView.accessibilityIdentifier = "Custom"
            calloutView.center = CGPoint(x: view.bounds.size.width / 2, y: -calloutView.bounds.size.height*0.52)
            view.addSubview(calloutView)
            mapView.setCenter((view.annotation?.coordinate)!, animated: true)
        }
    }
    
    func setPinColorForGroup(groupName: String) -> (UIColor?, UIImage?){
        var color: UIColor?
        var image: UIImage?
        switch groupName {
        case "Buehne":
            color = UIColor.black
            image = UIImage(named: "stage")
        case "EinAusgang":
            color = UIColor.blue
            image = nil
        case "Parkplatz":
            color = UIColor.green
            image = nil
        case "WC":
            image = UIImage(named: "WC")
        default:
            color = nil
            image = nil
        }
        
        return (color, image)
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if view.isKind(of: MKMarkerAnnotationView.self)
        {
            for views in view.subviews{
                if views.accessibilityIdentifier == "Custom"{
                    views.removeFromSuperview()
                }
            }
        }
    }
    
}
