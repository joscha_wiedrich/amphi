//
//  AmphiPlaces.swift
//  Amphi Festival
//
//  Created by Joscha Wiedrich on 12.06.18.
//  Copyright © 2018 Joscha Wiedrich. All rights reserved.
//

import MapKit

@objc class Places: NSObject, MKAnnotation{
    var title: String?
    var coordinate: CLLocationCoordinate2D
    var groupName: String?
    
    
    init(buehne: String?, coordinate: CLLocationCoordinate2D, groupName: String?){
        self.title = buehne
        self.coordinate = coordinate
        self.groupName = groupName
    }
    
    static func getPlaces() -> [Places]{
        guard let path = Bundle.main.path(forResource: "AmphiPlaces", ofType: "plist"), let array = NSArray(contentsOfFile: path) else{
            return []
        }
        
        var places = [Places]()
        
        for item in array{
            let dictionary = item as? [String: Any]
            
            if dictionary?["buehne"] != nil{
                let buehne =  dictionary?["buehne"] as? String
                let latitude = dictionary?["latitude"] as? Double ?? 0, longitude = dictionary?["longitude"] as? Double ?? 0
                let groupName = dictionary!["Group"] as? String
                
                let place = Places(buehne: buehne, coordinate: CLLocationCoordinate2D(latitude: latitude, longitude: longitude), groupName: groupName)
                
                places.append(place)
            }
            
            if dictionary?["EinAusgang"] != nil{
                let buehne =  dictionary?["EinAusgang"] as? String
                let latitude = dictionary?["latitude"] as? Double ?? 0, longitude = dictionary?["longitude"] as? Double ?? 0
                let groupName = dictionary?["Group"] as? String
                
                let place = Places(buehne: buehne, coordinate: CLLocationCoordinate2D(latitude: latitude, longitude: longitude), groupName: groupName)
                
                places.append(place)
            }
            if dictionary?["Parkplatz"] != nil{
                let buehne =  dictionary?["Parkplatz"] as? String
                let latitude = dictionary?["latitude"] as? Double ?? 0, longitude = dictionary?["longitude"] as? Double ?? 0
                let groupName = dictionary?["Group"] as? String
                
                let place = Places(buehne: buehne, coordinate: CLLocationCoordinate2D(latitude: latitude, longitude: longitude), groupName: groupName)
                
                places.append(place)
            }
            if dictionary?["WC"] != nil{
                let buehne =  dictionary?["WC"] as? String
                let latitude = dictionary?["latitude"] as? Double ?? 0, longitude = dictionary?["longitude"] as? Double ?? 0
                let groupName = dictionary?["Group"] as? String
                
                let place = Places(buehne: buehne, coordinate: CLLocationCoordinate2D(latitude: latitude, longitude: longitude), groupName: groupName)
                
                places.append(place)
            }
            
        }
        
        return places as [Places]
    }
    
    func mapItem() -> MKMapItem {
        let placemark = MKPlacemark(coordinate: coordinate)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        return mapItem
    }
}
