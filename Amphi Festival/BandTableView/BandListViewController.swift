//
//  BandListViewController.swift
//  Amphi Festival
//
//  Created by Joscha Wiedrich on 12.06.18.
//  Copyright © 2018 Joscha Wiedrich. All rights reserved.
//

import UIKit
import CoreData

class BandListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var searchActive : Bool = false
    var bandData: NSFetchedResultsController = BandData().fetchedResultsController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bandData.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        
        do{
            try bandData.performFetch()
        }catch{
            let fetchError = error as NSError
            print("Unable to load bands")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        do{
            try bandData.performFetch()
        }catch{
            let fetchError = error as NSError
            print("Unable to load bands")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - TableView
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = bandData.sections else{
            return 0
        }
        
        let sectionInfo = sections[section]
        return sectionInfo.numberOfObjects
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BandOverviewTableViewCell", for: indexPath) as! BandOverviewTableViewCell
        
        configureCell(cell, at: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func configureCell(_ cell: BandOverviewTableViewCell, at indexPath: IndexPath){
        let entry = bandData.object(at: indexPath)
        cell.bandImage?.image = StringDate.getImageFromUrl(imageData: entry.bandImage)
        cell.lbBandName.text = entry.bandName
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.performSegue(withIdentifier: "ShowBandDetailView", sender: self)
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowBandDetailView"{
            let detailView = segue.destination as! BandDetailViewController
            let indexPath = self.tableView.indexPathForSelectedRow
            let selectedBand = bandData.object(at: indexPath!)
            detailView.selectedBand = selectedBand
            
        }
    }
    
    func saveSwitchNotify(indexPath: IndexPath, switchState: Bool){
        let entry = bandData.object(at: indexPath)
        entry.setNotification = switchState
        do{
            try bandData.managedObjectContext.save()
        }catch{
            print("can´t save")
        }
    }
    
}
