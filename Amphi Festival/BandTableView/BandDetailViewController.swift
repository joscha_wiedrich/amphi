//
//  BandDetailViewController.swift
//  Amphi Festival
//
//  Created by Joscha Wiedrich on 12.06.18.
//  Copyright © 2018 Joscha Wiedrich. All rights reserved.
//

import UIKit

class BandDetailViewController: UIViewController {

    var selectedBand: Bands!
    
    @IBOutlet weak var bandImage: UIImageView?
    @IBOutlet weak var notifySwitch: UISwitch!
    @IBOutlet weak var lbBenachrichtung: UILabel!
    @IBOutlet weak var tvInfos: UITextView!
    
    @IBAction func notifySwitchChanged(_ sender: UISwitch) {
        if sender.isOn{
            selectedBand.setNotification = true
            lbBenachrichtung.text = "Du wirst rechtzeitig benachrichtigt"
            let infoString = LocalNotification.createNotificationInfoString(bandName: selectedBand.bandName!, startTime: selectedBand.playtimeStart!, endTime: selectedBand.playtimeEnd!, buehne: selectedBand.buehne!)
            LocalNotification.setNotification(notificationsForBand: selectedBand.bandName!, notificiationAdditionalInfos: infoString, notificationTime: selectedBand.playtimeStart!)
            
        }else{
            selectedBand.setNotification = false
            lbBenachrichtung.text = "An den Aufritt erinnern lassen"
            LocalNotification.deleteNotification(notificationsForBand: selectedBand.bandName!)
        }
        
        do{
            try selectedBand.managedObjectContext?.save()
        }catch{
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = selectedBand.bandName
        tvInfos.attributedText = selectedBand.bandInfo?.htmlToAttributedString
        bandImage?.image = StringDate.getImageFromUrl(imageData: selectedBand.bandImage)
        if selectedBand.setNotification == true{
            notifySwitch.isOn = true
            lbBenachrichtung.text = "Du wirst benachrichtigt"
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */


}
