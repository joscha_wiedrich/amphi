//
//  BandByDaysTableViewCell.swift
//  Amphi Festival
//
//  Created by Joscha Wiedrich on 12.06.18.
//  Copyright © 2018 Joscha Wiedrich. All rights reserved.
//

import UIKit

class BandByDaysTableViewCell: UITableViewCell {

    var callback: ((UITableViewCell, Bool) -> Void)?
    
    @IBOutlet weak var lbBandName: UILabel!
    @IBOutlet weak var lbTime: UILabel?
    @IBOutlet weak var lbBuehne: UILabel?
    @IBOutlet weak var ivBandImage: UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
