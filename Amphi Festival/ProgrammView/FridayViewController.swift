//
//  FridayViewController.swift
//  Amphi Festival
//
//  Created by Joscha Wiedrich on 12.06.18.
//  Copyright © 2018 Joscha Wiedrich. All rights reserved.
//

import UIKit
import CoreData

class FridayViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate {
    
    var bandsByDay = BandDataFriday().fetchedResultsController
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
        bandsByDay.delegate = self
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = bandsByDay.sections else{
            return 0
        }
        return sections[section].numberOfObjects
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sections = bandsByDay.sections else{
            return 0
        }
        
        return sections.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BandByDaysTableViewCell", for: indexPath) as! BandByDaysTableViewCell
        
        configureCell(cell, at: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let sections = bandsByDay.sections else{
            return nil
        }
        
        let sectionInfo = sections[section]
        return sectionInfo.name
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func configureCell(_ cell: BandByDaysTableViewCell, at indexPath: IndexPath){
        let entry = bandsByDay.object(at: indexPath)
        cell.lbBandName.text = entry.bandName
        let timeToPlay: String? = StringDate.CastDateFromCoreDataToString(date: entry.playtimeStart!) + " - " + StringDate.CastDateFromCoreDataToString(date: entry.playtimeEnd!)
        cell.lbTime?.text = timeToPlay
        cell.lbBuehne?.text = entry.buehne
        cell.ivBandImage?.image = StringDate.getImageFromUrl(imageData: entry.bandImage)
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowBandDetailView"{
            let detailView = segue.destination as! BandDetailViewController
            let indexPath = self.tableView.indexPathForSelectedRow
            let selectedBand = bandsByDay.object(at: indexPath!)
            detailView.selectedBand = selectedBand
            
        }
    }
    
}
